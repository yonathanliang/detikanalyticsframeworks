//
//  DAI_SCREEN+CoreDataClass.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 13/02/18.
//  Copyright © 2018 detik. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DAI_TRACKER;

NS_ASSUME_NONNULL_BEGIN

@interface DAI_SCREEN : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "DAI_SCREEN+CoreDataProperties.h"
