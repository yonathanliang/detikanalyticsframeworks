//
//  DetikAnalytics.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 8/6/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DAIDataManager.h"
#import "DAIConnectionManager.h"

#define kDAIDefaultDataSendInformation 600 //in seconds

@class DetikAnalytics;

@interface DetikAnalytics : NSObject

@property (nonatomic, strong)NSString *baseURL;
@property (nonatomic, strong)DAI_TRACKER *tracker;
@property (nonatomic, strong)DAIDataManager *dataManager;
@property (nonatomic, strong)DAIConnectionManager *connectionManager;
@property (nonatomic, strong)NSString *connectID;
/*singleton*/

+ (DetikAnalytics *)sharedInstance;
/* set device information into it's metadata table
 if device information already exists on table, return false boolean
 */
- (BOOL)setDeviceInformations;

#pragma mark - methods
/* do things on application launch */
- (void)start;
/* get currently active screen */
- (DAI_SCREEN *)getCurrentScreen;
/* generate screen regardless of there is an active screen */
- (DAI_SCREEN *)prepareScreen;
/* send screen */
- (void)sendScreen:(DAI_SCREEN *)screen;
/* immediately generate screen and send data along with it */
- (DAI_SCREEN *)sendScreenInfo:(NSDictionary *)screenInfo;
/* end active screen */
- (void)endScreen:(DAI_SCREEN *)screen;
/* send event */
- (void)sendEvent:(NSDictionary *)event;
#pragma mark - setter
/* set tracker's session */
- (void)setInterval:(int)varInterval;

#pragma mark - data persisting
/* save context */
- (void)save;


#pragma mark - testing purpose
/* testing purpose : show all tracker and the screen within it */
- (void)showAllTracker;

@end
