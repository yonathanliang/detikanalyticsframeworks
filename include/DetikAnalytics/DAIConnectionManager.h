//
//  DAIConnectionManager.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 8/20/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DAIDataManager;
@class DAI_TRACKER;

@interface DAIConnectionManager : NSObject

@property (nonatomic, strong)DAIDataManager *dataManager;

- (void)sendQueuedTracker;
@end
