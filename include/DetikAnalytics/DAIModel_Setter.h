//
//  DAIModel_Setter.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 8/14/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DAI_SCREEN;

@interface DAIModel_Setter : NSObject
+ (BOOL)isNull:(id)object;

+ (void)fillScreen:(DAI_SCREEN *)screen with:(NSDictionary *)info;
@end
