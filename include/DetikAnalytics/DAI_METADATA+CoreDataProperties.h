//
//  DAI_METADATA+CoreDataProperties.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 13/02/18.
//  Copyright © 2018 detik. All rights reserved.
//
//

#import "DAI_METADATA+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DAI_METADATA (CoreDataProperties)

+ (NSFetchRequest<DAI_METADATA *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *detik_id;
@property (nullable, nonatomic, copy) NSString *device_brand;
@property (nullable, nonatomic, copy) NSString *device_id;
@property (nullable, nonatomic, copy) NSString *device_name;
@property (nullable, nonatomic, copy) NSString *device_os;
@property (nullable, nonatomic, copy) NSString *screen_resolution;
@property (nullable, nonatomic, copy) NSString *app_version;
@property (nullable, nonatomic, copy) NSString *sdk_version;

@end

NS_ASSUME_NONNULL_END
