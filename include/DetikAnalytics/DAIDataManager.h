//
//  DAIDataManager.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 8/7/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "DAI_METADATA+CoreDataClass.h"
#import "DAI_EVENT.h"
#import "DAI_SCREEN+CoreDataClass.h"
#import "DAI_TRACKER.h"

@interface DAIDataManager : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)showAllTrackerAndScreen;
- (DAI_TRACKER *)createNewTracker;
- (DAI_TRACKER *)validRunningTracker;
- (DAI_TRACKER *)getOldestTracker;
- (void)deleteTracker:(DAI_TRACKER *)tracker;
- (DAI_SCREEN *)getNewScreen;
- (BOOL)checkTrackerSessionValidity:(DAI_TRACKER *)varTracker;

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectModel *)managedObjectModel;
- (NSManagedObjectContext *)managedObjectContext;

- (DAI_METADATA *)getMetaData;
- (BOOL)storeDeviceInformation:(NSDictionary *)deviceInformation;
- (BOOL)save;
@end
