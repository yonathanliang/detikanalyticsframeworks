//
//  DAI_TRACKER.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 9/1/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DAI_SCREEN;

@interface DAI_TRACKER : NSManagedObject

@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * end_time_stamp;
@property (nonatomic, retain) NSString * start_time_stamp;
@property (nonatomic, retain) NSData * event;
@property (nonatomic, retain) NSSet *screenviews;
@end

@interface DAI_TRACKER (CoreDataGeneratedAccessors)

- (void)addScreenviewsObject:(DAI_SCREEN *)value;
- (void)removeScreenviewsObject:(DAI_SCREEN *)value;
- (void)addScreenviews:(NSSet *)values;
- (void)removeScreenviews:(NSSet *)values;

@end
