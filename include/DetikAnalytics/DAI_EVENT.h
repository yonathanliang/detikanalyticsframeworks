//
//  DAI_EVENT.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 8/12/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DAI_TRACKER;

@interface DAI_EVENT : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) DAI_TRACKER *tracker;

@end
