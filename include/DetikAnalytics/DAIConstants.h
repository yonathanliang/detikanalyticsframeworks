//
//  DAIConstants.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 8/13/15.
//  Copyright (c) 2015 detik. All rights reserved.
//

#ifndef DetikAnalytics_DAIConstants_h
#define DetikAnalytics_DAIConstants_h

static NSString * const kDAIModelBundleName = @"DAIDataModels";
static NSString * const kDAIModelName = @"DAIDataModel";

static NSString * const kEntityNameMetaData = @"DAI_METADATA";
static NSString * const kEntityNameTracker = @"DAI_TRACKER";
static NSString * const kEntityNameScreen = @"DAI_SCREEN";
static NSString * const kEntityNameEvent = @"DAI_EVENT";

static NSString * const kSDKVersion = @"2.0.0";

//static NSString

//information keys
#define KDAIDeviceInfoDevice_ID         @"device id"
#define KDAIDeviceInfoDevice_Name       @"device name"
#define kDAIDeviceInfoDevice_Brand      @"device brand"
#define kDAIDeviceInfoDevice_OS         @"device os"
#define kDAIDeviceInfoScreen_Resolution @"screen resolution"
#define KDAIDeviceInfoAppVersion        @"app version"
#define kDAIDeviceSDKVersion            @"sdk version"

#define kDAIScreen_ArticleID            @"Screen Article ID"
#define kDAIScreen_DateCreated          @"Screen Date Created"
#define kDAIScreen_EndTime              @"Screen End Time"
#define kDAIScreen_StartTime            @"Screen Start Time"
#define kDAIScreen_KanalID              @"Screen Kanal ID"
#define kDAIScreen_PageNumber           @"Screen Page Number"
#define kDAIScreen_PageSize             @"Screen Page Size"
#define kDAIScreen_PageType             @"Screen Page Type"
#define kDAIScreen_Title                @"Screen Title"

//logging
#define kDAILog_SuccessSavingContext    @"DAI-Succeeded saving context"
#define kDAILog_FailedSavingContext     @"DAI-Failed saving context"
#define kDAILog_Exception(exc)          @"DAI-EXC : %@", exc

#define kDAILog_ERROR(ERROR)            @"DAI-ERR : %@", ERROR
#define kDAILog_ERRNoTracker            @"DAI-ERR: No Tracker Founded"
#define kDAILog_ERRNoScreenInTracker    @"DAI-ERR: No Screen founded in tracker"
#define kDAILog_ERRConnectionOnGoing    @"DAI-ERR : Cancel sending tracker, already in the middle of sending data"

#define kDAILog_ConnStart               @"DAI-Connection Started"
#define kDAILog_ConnFinished            @"DAI-Connection Finished"
#define kDAILog_ConnFailed(err)         @"DAI-Connection Failed : %@", err
#define kDAILog_ConnResponded(resp)     @"DAI-Connection Received Response : %@", resp
#define kDAILog_ConnReceiveData(data)   @"DAI-Connection Received Data : %@", data
#define kDAILog_ConnSendDataByte(kB)    @"DAI-Connection Sent : %lu kB", kB


#endif
