//
//  DAI_SCREEN+CoreDataProperties.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 13/02/18.
//  Copyright © 2018 detik. All rights reserved.
//
//

#import "DAI_SCREEN+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DAI_SCREEN (CoreDataProperties)

+ (NSFetchRequest<DAI_SCREEN *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *account_code;
@property (nullable, nonatomic, copy) NSString *article_id;
@property (nullable, nonatomic, copy) NSString *connect_id;
@property (nullable, nonatomic, retain) NSData *customValues;
@property (nullable, nonatomic, copy) NSString *date_created_time_stamp;
@property (nullable, nonatomic, copy) NSString *end_time_stamp;
@property (nullable, nonatomic, copy) NSString *kanal_id;
@property (nullable, nonatomic, copy) NSString *screenName;
@property (nullable, nonatomic, copy) NSString *start_time_stamp;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *url;
@property (nullable, nonatomic, copy) NSString *date_published_time_stamp;
@property (nullable, nonatomic, retain) DAI_TRACKER *tracker;

@end

NS_ASSUME_NONNULL_END
