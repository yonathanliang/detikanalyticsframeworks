//
//  DAI_METADATA+CoreDataClass.h
//  DetikAnalytics
//
//  Created by yonathan justianus muliawan on 13/02/18.
//  Copyright © 2018 detik. All rights reserved.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface DAI_METADATA : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "DAI_METADATA+CoreDataProperties.h"
